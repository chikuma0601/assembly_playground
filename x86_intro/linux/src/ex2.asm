global _start

section .data
    msg db "Hello, World!", 0x0a   ; inline data in for referencing
    len equ $ - msg                ; length of the string $ means here

section .text
_start:
    mov eax, 4  ; sys_write
    mov ebx, 1  ; stdout file descriptor
    mov ecx, msg
    mov edx, len    ; number of bytes to write
    int 0x80
    mov eax, 1
    mov ebx, 0
    syscall
