# Intro to x86 assembly language

The reference of this part is from: [Portal](https://youtu.be/wLXIWKUWpSs) by Davy Wybiral.

## To compile
### On MacOS
```
nasm -f macho64 ex1.asm -o ex1.o
ld -macosx_version_min 10.7.0 -lSystem -o ex1 ex1.o
```
#### Notes of Compiling on MacOS
MacOS Catalina does not support 32-bit anymore :(
Kernel calling convention on MacOS x86_64: [](https://stackoverflow.com/questions/40814507/ld-linker-error-undefined-symbols-for-architecture-x86-64)

#### On Linux (32-bit)
```
nasm -f elf32 ex1.asm -o ex1.o
ld -m elf_i386 ex1.o -o ex1
```

## Basic operations
* Basic arithmetic operations
```
mov ebx, 123    ; ebx = 123
mov eax, ebx    ; eax = ebx
add ebx, ecx    ; ebx += ecx
sub ebx, edx    ; ebx -= edx
mul ebx         ; eax *= ebx
div edx         ; eax /= edx
```
Note that `MUL` and `DIV` also affect `EDX`. The `MUL` instruction stores the higher half of the result in `EDX`, while `DIV` stores the remainder from division there. If one dosn't know about that, one can be very surprised that suddenly their `EDX` is getting clobbered with "random" numbers after division/multiplication.

* Jump operations
```
je  A, B    ; jump if =
jne A, B    ; jump if !=
jg  A, B    ; jump is >
jge A, B    ; jump if >=
jl  A, B    ; jump if <
jle A, B    ; jump if <=
```
