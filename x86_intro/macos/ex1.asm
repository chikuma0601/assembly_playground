global start
section .text

start:
    mov eax, 0x2000001      ; system exit call
    mov edi, 42             ; exit status (same as return 42)
    sub edi, 10
    syscall                 ; interrupt handler for system call (instead of int 0x80 in macos)
