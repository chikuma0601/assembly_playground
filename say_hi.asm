SECTION .DATA
    hello:      db 'Hello, World!',10
    hellolen:   equ $-hello

SECTION .TEXT
    GLOBAL say_hi

say_hi:
    mov eax, 4 ; write()
    mov ebx, 1 ; STDOUT
    mov ecx, hello
    mov edx, hellolen
    int 80h
    ret
